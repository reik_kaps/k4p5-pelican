#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from time import strftime
import sys
import os

sys.path.append(os.curdir)
from custom_filter import md5hash

JINJA_FILTERS = {'md5hash': md5hash}

NOW = strftime('%c')

AUTHOR = 'Reiko Kaps'
SITENAME = 'k4p5'
SITESUBTITLE = 'meistens meta'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'de'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('Jan-Piet Mens', 'https://jpmens.net'),
    ('Carsten Strotmann', 'https://strotmann.de'),
    ('Paul Cochrane', 'https://ptc-it.de'),
    ('Christian Imhorst', 'https://www.datenteiler.de'),
)

# Social widget
SOCIAL = (
    ('mastodon', 'https://mastodon.social/@reik_kaps'),
    ('xmpp', 'xmpp://reik@mutt365.de'),
    ('codeberg', 'https://codeberg.org/reik_kaps'),
    ('github', 'https://github.com/reikkaps'),
)

# Links to own sites and services
INTERNAL = (
    ('Nextcloud', 'https://nc.k4p5.de'),
    ('Gallery', 'https://k4p5.de/gallery'),
)

DEFAULT_PAGINATION = 10
SUMMARY_MAX_LENGTH = 200

LICENSE = 'CC BY-SA 4.0'

# activate plugins
# PLUGINS =['pelican.plugins.webassets']



# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# solar theme and plugins
THEME = "themes/solar/solar"
PLUGIN_PATHS = ['plugins/solar-plugins']


PLUGINS = ['assets',
           'related_posts',
           'image_process'
           ]


STABLE_SITEURL = ''

# dither
THRESHOLD = [170,170,170]
DITHER_PALETTE = [(0,0,0),(25,25,25),(50,50,50),(75,75,75),(100,100,100),(125,125,125),(150,150,150),(175,175,175),(200,200,200),(225,225,225),(250,250,250)]
MAX_SIZE = [1024,1024]

# image-process
IMAGE_PROCESS = {
    "featured" : {
        'type': 'image',
        'ops': ["scale_out 300 300 True",
                "detail",
                "grayscale",
                "sharpen"],
    },
    "crisp": {
        "type": "responsive-image",
        "srcset": [
            ("1x", ["scale_in 800 600 True"]),
            ("2x", ["scale_in 1600 1200 True"]),
            ("4x", ["scale_in 3200 2400 True"]),
        ],
        "default": "1x",
    },
    "large-photo": {
        "type": "responsive-image",
        "sizes": (
            "(min-width: 1600px) 1200px, "
            "(min-width: 1200px) 800px, "
            "(min-width: 992px) 650px, "
            "(min-width: 768px) 718px, "
            "100vw"
        ),
        "srcset": [
            ("600w", ["scale_in 600 450 True"]),
            ("800w", ["scale_in 800 600 True"]),
            ("1600w", ["scale_in 1600 1200 True"]),
        ],
        "default": "800w",
    },
}

# plugin photos
PHOTO_LIBRARY = "~/Bilder/online/"
PHOTO_ARTICLE = (1024, 768, 90) # For photos associated with articles, maximum width, height, and quality. The maximum size would typically depend on the needs of the theme. 760px is suitable for the theme notmyidea.
PHOTO_THUMB = (384, 288, 70) # For thumbnails, maximum width, height, and quality.
PHOTO_SQUARE_THUMB = True #  Crops thumbnails to make them square.
PHOTO_RESIZE_JOBS = 2 # Number of parallel resize jobs to be run. Defaults to 1.
PHOTO_WATERMARK = True  # Adds a watermark to all photos in articles and pages. Defaults to using your site name.
PHOTO_WATERMARK_TEXT = SITENAME # Allow the user to change the watermark text or remove it completely. By default it uses SourceCodePro-Bold as the font.
PHOTO_WATERMARK_IMG = '' # Allows the user to add an image in addition to or as the only watermark. Set the variable to the location.

# needed piexif module
PHOTO_EXIF_KEEP = True
# PHOTO_EXIF_REMOVE_GPS = True #  Removes any GPS information from the files exif data.
# PHOTO_EXIF_COPYRIGHT = 'CC0' # Attaches an author and a license to the file. Choices include: - COPYRIGHT: Copyright - CC0: Public Domain - CC-BY-NC-ND: Creative Commons Attribution-NonCommercial-NoDerivatives - CC-BY-NC-SA: Creative Commons Attribution-NonCommercial-ShareAlike - CC-BY: Creative Commons Attribution - CC-BY-SA: Creative Commons Attribution-ShareAlike - CC-BY-NC: Creative Commons Attribution-NonCommercial - CC-BY-ND: Creative Commons Attribution-NoDerivatives

# PHOTO_EXIF_COPYRIGHT_AUTHOR = 'Reiko Kaps' # Adds an author name to the photo's exif and copyright statement. Defaults to AUTHOR value from the pelicanconf.py
# PHOTO_INLINE_GALLERY_ENABLED #  Enable inline gallery processing. (Default: False)
# PHOTO_INLINE_GALLERY_PATTERN #  The pattern to look for. The gallery_name is used to find the right gallery.


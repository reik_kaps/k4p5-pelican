FROM python:3.11-bookworm

LABEL Version="0.1b" \
    Date="2023-Nov-10" \
    Docker_Version="23.0.1, build a5ee5b1" \
    Maintainer="Reiko Kaps @reik_kaps" \
    Description="A basic Docker container to compile my pelican weblog"

RUN python -m pip install "pelican[markdown]"

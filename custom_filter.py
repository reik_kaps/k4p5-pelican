# custom jinja2 filter
# @see https://jinja.palletsprojects.com/en/latest/api/#custom-filters

import hashlib

def md5hash(path):
    print('>>> {}'.format(str(path)))
    return hashlib.md5(str(path).encode()).hexdigest()


